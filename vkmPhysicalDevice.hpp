#ifndef VKM_PHYSICAL_DEVICE
#define VKM_PHYSICAL_DEVICE

#include <vulkan/vulkan.h>
#include <assert.h>
#include <string>
#include <iostream>

#include "vkmTools.hpp"


class vkmPhysicalDevice
{
private:
    VkPhysicalDeviceProperties vk_physicalDeviceProperties;
    VkPhysicalDeviceMemoryProperties vk_physicalDeviceMemoryProperties;
protected:
public:

    VkPhysicalDevice vk_physicalDevice;
    VkSurfaceKHR vk_surfaceKHR;
    uint32_t graphicsQueueFamilyIndex;
    uint32_t presentQueueFamilyIndex;

    explicit vkmPhysicalDevice(const VkPhysicalDevice physicalDevice, const VkSurfaceKHR surface) : vk_physicalDevice(physicalDevice), vk_surfaceKHR(surface)
    {
        // Obtain Physical Device Properties

        vkGetPhysicalDeviceProperties(vk_physicalDevice, &vk_physicalDeviceProperties);
        vkGetPhysicalDeviceMemoryProperties(vk_physicalDevice, &vk_physicalDeviceMemoryProperties);

        // Obtain Queue Family Indices

        uint32_t numQueueFamilyProperties = 0;
        vkGetPhysicalDeviceQueueFamilyProperties(vk_physicalDevice, &numQueueFamilyProperties, nullptr);
        VkQueueFamilyProperties* queueFamilyProperties = new VkQueueFamilyProperties[numQueueFamilyProperties];
        vkGetPhysicalDeviceQueueFamilyProperties(vk_physicalDevice, &numQueueFamilyProperties, queueFamilyProperties);

        bool graphics_and_present_found = false;
        for (uint32_t i = 0; i < numQueueFamilyProperties; ++i)
        {
            VkBool32 q_fam_supports_present = false;
            vkGetPhysicalDeviceSurfaceSupportKHR(vk_physicalDevice, i, surface, &q_fam_supports_present);

            if (queueFamilyProperties[i].queueFlags & VK_QUEUE_GRAPHICS_BIT && q_fam_supports_present == VK_TRUE)
            {
                graphicsQueueFamilyIndex = i;
                presentQueueFamilyIndex  = i;

                std::cout << "minImageTransgerGranulariry: (" 
                          << queueFamilyProperties[i].minImageTransferGranularity.width << ", "
                          << queueFamilyProperties[i].minImageTransferGranularity.height << ", "
                          << queueFamilyProperties[i].minImageTransferGranularity.depth << ")\n";
                break;
            }
        }

        delete [] queueFamilyProperties;

        assert(graphicsQueueFamilyIndex < UINT32_MAX && "no supported graphics queue family index");
        assert(presentQueueFamilyIndex  < UINT32_MAX && "no supported present queue family index");
    }

    ~vkmPhysicalDevice() = default;

    uint32_t getHeapIndex(const uint32_t memoryTypeIndices, const VkMemoryPropertyFlags memoryPropertyFlags) const
    {
    	// Iterate over all memory types available for the device used in this example
    	for (uint32_t i = 0; i < vk_physicalDeviceMemoryProperties.memoryTypeCount; i++)
    	{
    		if (memoryTypeIndices & (1 << i) && (vk_physicalDeviceMemoryProperties.memoryTypes[i].propertyFlags & memoryPropertyFlags) == memoryPropertyFlags)
    		{
    			return i;
    		}
    	}

        assert( false && "Could not find suitable memory type!");
        return 0;
    }

    void show_vkPhysicalDeviceMemoryProperties()
    {
        size_t i;
        std::string table[VK_MAX_MEMORY_HEAPS];

        for (i = 0; i < vk_physicalDeviceMemoryProperties.memoryTypeCount; i++)
        {
            const std::string memoryPropertyFlags = convert(VkType::VK_MEMORY_PROPERTY_FLAG, vk_physicalDeviceMemoryProperties.memoryTypes[i].propertyFlags);
            table[vk_physicalDeviceMemoryProperties.memoryTypes[i].heapIndex] = memoryPropertyFlags;
        }

        std::cout << "Physical Device Memory Properties: " << vk_physicalDeviceProperties.deviceName << '\n';

        for (i = 0; i < vk_physicalDeviceMemoryProperties.memoryHeapCount; i++)
        {
            const std::string memoryHeapFlags = convert(VkType::VK_MEMORY_HEAP_FLAG, vk_physicalDeviceMemoryProperties.memoryHeaps[i].flags);

            std::cout << "Heap " << i << '\n';
            std::cout << "\t.size = " << static_cast<size_t>(vk_physicalDeviceMemoryProperties.memoryHeaps[i].size / 1000000) << " MB\n"; 
            std::cout << "\t.memoryHeapFlags = " << memoryHeapFlags << '\n';
            std::cout << "\t.memoryPropertyFlags = " << table[i] << '\n';
        }
        std::cout << '\n';
    }
};

#endif // VKM_PHYSICAL_DEVICE