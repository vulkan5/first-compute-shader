/*
@licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2019 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Khronos Texture Tools", "index.html", [
    [ "ktxinfo", "ktxinfo.html", [
      [ "SYNOPSIS", "ktxinfo.html#ktxinfo_synopsis", null ],
      [ "DESCRIPTION", "ktxinfo.html#ktxinfo_description", null ],
      [ "EXIT STATUS", "ktxinfo.html#ktxinfo_exitstatus", null ],
      [ "HISTORY", "ktxinfo.html#ktxinfo_history", null ],
      [ "AUTHOR", "ktxinfo.html#ktxinfo_author", null ]
    ] ],
    [ "ktx2check", "ktx2check.html", [
      [ "SYNOPSIS", "ktx2check.html#ktx2check_synopsis", null ],
      [ "DESCRIPTION", "ktx2check.html#ktx2check_description", null ],
      [ "EXIT STATUS", "ktx2check.html#ktx2check_exitstatus", null ],
      [ "HISTORY", "ktx2check.html#ktx2check_history", null ],
      [ "AUTHOR", "ktx2check.html#ktx2check_author", null ]
    ] ],
    [ "ktx2ktx2", "ktx2ktx2.html", [
      [ "SYNOPSIS", "ktx2ktx2.html#ktx2ktx2_synopsis", null ],
      [ "DESCRIPTION", "ktx2ktx2.html#ktx2ktx2_description", null ],
      [ "EXIT STATUS", "ktx2ktx2.html#ktx2ktx2_exitstatus", null ],
      [ "HISTORY", "ktx2ktx2.html#ktx2ktx2_history", null ],
      [ "AUTHOR", "ktx2ktx2.html#ktx2ktx2_author", null ]
    ] ],
    [ "ktxsc", "ktxsc.html", [
      [ "SYNOPSIS", "ktxsc.html#ktxsc_synopsis", null ],
      [ "DESCRIPTION", "ktxsc.html#ktxsc_description", null ],
      [ "EXIT STATUS", "ktxsc.html#ktxsc_exitstatus", null ],
      [ "HISTORY", "ktxsc.html#ktxsc_history", null ],
      [ "AUTHOR", "ktxsc.html#ktxsc_author", null ]
    ] ],
    [ "toktx", "toktx.html", [
      [ "SYNOPSIS", "toktx.html#toktx_synopsis", null ],
      [ "DESCRIPTION", "toktx.html#toktx_description", null ],
      [ "EXIT STATUS", "toktx.html#toktx_exitstatus", null ],
      [ "HISTORY", "toktx.html#toktx_history", null ],
      [ "AUTHOR", "toktx.html#toktx_author", null ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"index.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';