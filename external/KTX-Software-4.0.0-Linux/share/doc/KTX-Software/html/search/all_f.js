var searchData=
[
  ['pallocator_157',['pAllocator',['libktx/structktxVulkanDeviceInfo.html#ab1067ca1dda7c8aa2da946e867622c1e',1,'ktxVulkanDeviceInfo']]],
  ['pdata_158',['pData',['libktx/structktxTexture.html#a979bf5b23162095d6ca6ba720b134f21',1,'ktxTexture']]],
  ['pdfd_159',['pDfd',['libktx/structktxTextureCreateInfo.html#a0d39d6c4f004b3d1314d9684ffc3d8b4',1,'ktxTextureCreateInfo']]],
  ['pfnktxitercb_160',['PFNKTXITERCB',['libktx/structktxTexture.html#ac17c25cbbbd8fab648b55eced7111b96',1,'ktxTexture']]],
  ['physicaldevice_161',['physicalDevice',['libktx/structktxVulkanDeviceInfo.html#ac59a6dca1cb19c69dc83eb2271088a0a',1,'ktxVulkanDeviceInfo']]],
  ['preswizzle_162',['preSwizzle',['libktx/structktxBasisParams.html#ab6e76b713d00fe3784d46eb768262616',1,'ktxBasisParams']]]
];
