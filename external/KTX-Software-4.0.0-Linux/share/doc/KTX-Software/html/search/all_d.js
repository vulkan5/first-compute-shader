var searchData=
[
  ['noendpointrdo_147',['noEndpointRDO',['libktx/structktxBasisParams.html#a24951ae41577c7f0bfdf42c122584ac7',1,'ktxBasisParams']]],
  ['normalmap_148',['normalMap',['libktx/structktxBasisParams.html#af2594a3262044caa4b7896f20c260ea0',1,'ktxBasisParams']]],
  ['noselectorrdo_149',['noSelectorRDO',['libktx/structktxBasisParams.html#ad5da44a6855e532ae2466f9f412ce6c6',1,'ktxBasisParams']]],
  ['nosse_150',['noSSE',['libktx/structktxBasisParams.html#a20c1b813b1298d8e6d1b13232bbe2f46',1,'ktxBasisParams']]],
  ['numdimensions_151',['numDimensions',['libktx/structktxTexture.html#a314ae942ddb59643777ec4ba040fd448',1,'ktxTexture::numDimensions()'],['libktx/structktxTextureCreateInfo.html#aee982947789840643580ef13d9209124',1,'ktxTextureCreateInfo::numDimensions()']]],
  ['numfaces_152',['numFaces',['libktx/structktxTexture.html#a46234ef09e2c28591ab4654f8eac12a0',1,'ktxTexture::numFaces()'],['libktx/structktxTextureCreateInfo.html#ad0f09614599bd18f0430d2e3e3f2acf1',1,'ktxTextureCreateInfo::numFaces()']]],
  ['numlayers_153',['numLayers',['libktx/structktxTextureCreateInfo.html#a237cfa756c1b49169d769442c1c5492a',1,'ktxTextureCreateInfo']]],
  ['numlevels_154',['numLevels',['libktx/structktxTexture.html#a2b452d1475b8d246839882e26fe3203b',1,'ktxTexture::numLevels()'],['libktx/structktxTextureCreateInfo.html#a5bc01f6ce3a1082921cc307d80b4662c',1,'ktxTextureCreateInfo::numLevels()']]]
];
