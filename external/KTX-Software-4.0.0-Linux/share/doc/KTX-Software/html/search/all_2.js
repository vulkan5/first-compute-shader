var searchData=
[
  ['basedepth_4',['baseDepth',['libktx/structktxTexture.html#a195423d7a23d2a01243379978e0aab37',1,'ktxTexture::baseDepth()'],['libktx/structktxTextureCreateInfo.html#acd631299f24ac52bc6af9d9303251a0e',1,'ktxTextureCreateInfo::baseDepth()']]],
  ['baseheight_5',['baseHeight',['libktx/structktxTexture.html#af8674d96c821b560d9116ebb48a926aa',1,'ktxTexture::baseHeight()'],['libktx/structktxTextureCreateInfo.html#af401308b301717196a51015aac339e90',1,'ktxTextureCreateInfo::baseHeight()']]],
  ['basewidth_6',['baseWidth',['libktx/structktxTexture.html#ac8470a8c8775a60fe5072e63998aadcb',1,'ktxTexture::baseWidth()'],['libktx/structktxTextureCreateInfo.html#ae817666a8c8ef8fbed5d81b52c421b9e',1,'ktxTextureCreateInfo::baseWidth()']]],
  ['basis_20image_20transcoder_20binding_7',['Basis Image Transcoder binding',['../msc_basis_transcoder.html',1,'js_bindings']]]
];
