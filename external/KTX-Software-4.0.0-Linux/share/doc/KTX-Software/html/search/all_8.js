var searchData=
[
  ['image_24',['image',['libktx/structktxVulkanTexture.html#a8e1c1e2f71c9921d1cb47d50a6e7a8cb',1,'ktxVulkanTexture']]],
  ['imageformat_25',['imageFormat',['libktx/structktxVulkanTexture.html#a51a2976a43d1c449904208ab52ee217f',1,'ktxVulkanTexture']]],
  ['imagelayout_26',['imageLayout',['libktx/structktxVulkanTexture.html#ad86553423ca7b214849b6a26bd1c4010',1,'ktxVulkanTexture']]],
  ['inputswizzle_27',['inputSwizzle',['libktx/structktxBasisParams.html#a2b3befb6617cde21ca3e5b222257c66c',1,'ktxBasisParams']]],
  ['isarray_28',['isArray',['libktx/structktxTexture.html#ad18302edd81e0368b93d7fdd517813e8',1,'ktxTexture::isArray()'],['libktx/structktxTextureCreateInfo.html#a7f6d6379fcb54b95769000b3919a659b',1,'ktxTextureCreateInfo::isArray()']]],
  ['iscubemap_29',['isCubemap',['libktx/structktxTexture.html#a4e9ae7f75a7e1f05ca4f9eacba84db82',1,'ktxTexture']]]
];
