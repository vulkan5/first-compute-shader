var structktxVulkanDeviceInfo =
[
    [ "ktxVulkanDeviceInfo_Construct", "group__ktx__vkloader.html#ga4bc9f0fa9af93d588276f54fe9a6ba50", null ],
    [ "ktxVulkanDeviceInfo_Create", "group__ktx__vkloader.html#ga82ac7e21e884652c519d9fe28ad5428c", null ],
    [ "ktxVulkanDeviceInfo_Destroy", "group__ktx__vkloader.html#gaaf633943fbf201fb620c0270c6150fa8", null ],
    [ "ktxVulkanDeviceInfo_Destruct", "group__ktx__vkloader.html#gae58928740420d1ed3f96fd4b0f2d897e", null ],
    [ "cmdBuffer", "structktxVulkanDeviceInfo.html#a94ecf9f8949d967b51bc0b381f7e23ae", null ],
    [ "cmdPool", "structktxVulkanDeviceInfo.html#ac53a30dcd52922f0e581cc72e4c6d04a", null ],
    [ "device", "structktxVulkanDeviceInfo.html#a6021332b7544982e536f1b88d2758999", null ],
    [ "deviceMemoryProperties", "structktxVulkanDeviceInfo.html#afa5fb09215087b5ff27022eeab52c7e8", null ],
    [ "pAllocator", "structktxVulkanDeviceInfo.html#ab1067ca1dda7c8aa2da946e867622c1e", null ],
    [ "physicalDevice", "structktxVulkanDeviceInfo.html#ac59a6dca1cb19c69dc83eb2271088a0a", null ],
    [ "queue", "structktxVulkanDeviceInfo.html#af601e4d82e769a68d7f0a8bcccf3e365", null ]
];