var structktxVulkanTexture =
[
    [ "ktxVulkanTexture_Destruct", "group__ktx__vkloader.html#gaf620a84f6bc59bff03b521e55a654f19", null ],
    [ "depth", "structktxVulkanTexture.html#af990e0fd73d55500f48d616a0f35a470", null ],
    [ "deviceMemory", "structktxVulkanTexture.html#ae78e0952193debfc52b76f2978cca58c", null ],
    [ "height", "structktxVulkanTexture.html#aaf35829f0694dedb8e05f19e787301c1", null ],
    [ "image", "structktxVulkanTexture.html#a8e1c1e2f71c9921d1cb47d50a6e7a8cb", null ],
    [ "imageFormat", "structktxVulkanTexture.html#a51a2976a43d1c449904208ab52ee217f", null ],
    [ "imageLayout", "structktxVulkanTexture.html#ad86553423ca7b214849b6a26bd1c4010", null ],
    [ "layerCount", "structktxVulkanTexture.html#a7e7fcaf13c3c30d6046137e21a1920d4", null ],
    [ "levelCount", "structktxVulkanTexture.html#afce4a647ee2aa280f0e6957ecc520d43", null ],
    [ "viewType", "structktxVulkanTexture.html#a03ccdaea9ac14f8ebfdf53cc0bbed379", null ],
    [ "width", "structktxVulkanTexture.html#a7b9da58e222e1f4952d0c570f6419aa6", null ]
];