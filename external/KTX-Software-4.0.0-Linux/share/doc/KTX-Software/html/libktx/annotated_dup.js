var annotated_dup =
[
    [ "ktxBasisParams", "structktxBasisParams.html", "structktxBasisParams" ],
    [ "ktxHashList", "classktxHashList.html", "classktxHashList" ],
    [ "ktxHashListEntry", "classktxHashListEntry.html", "classktxHashListEntry" ],
    [ "ktxOrientation", "structktxOrientation.html", "structktxOrientation" ],
    [ "ktxTexture", "structktxTexture.html", "structktxTexture" ],
    [ "ktxTexture1", "structktxTexture1.html", "structktxTexture1" ],
    [ "ktxTexture2", "structktxTexture2.html", "structktxTexture2" ],
    [ "ktxTexture_vtbl", "structktxTexture__vtbl.html", null ],
    [ "ktxTextureCreateInfo", "structktxTextureCreateInfo.html", "structktxTextureCreateInfo" ],
    [ "ktxVulkanDeviceInfo", "structktxVulkanDeviceInfo.html", "structktxVulkanDeviceInfo" ],
    [ "ktxVulkanTexture", "structktxVulkanTexture.html", "structktxVulkanTexture" ]
];