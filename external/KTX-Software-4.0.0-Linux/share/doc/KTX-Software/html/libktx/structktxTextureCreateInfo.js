var structktxTextureCreateInfo =
[
    [ "baseDepth", "structktxTextureCreateInfo.html#acd631299f24ac52bc6af9d9303251a0e", null ],
    [ "baseHeight", "structktxTextureCreateInfo.html#af401308b301717196a51015aac339e90", null ],
    [ "baseWidth", "structktxTextureCreateInfo.html#ae817666a8c8ef8fbed5d81b52c421b9e", null ],
    [ "generateMipmaps", "structktxTextureCreateInfo.html#ae77eda256cb25882c635ca7515811f0e", null ],
    [ "glInternalformat", "structktxTextureCreateInfo.html#a6d1bde0fd4b46618d1f15aa519e08c6b", null ],
    [ "isArray", "structktxTextureCreateInfo.html#a7f6d6379fcb54b95769000b3919a659b", null ],
    [ "numDimensions", "structktxTextureCreateInfo.html#aee982947789840643580ef13d9209124", null ],
    [ "numFaces", "structktxTextureCreateInfo.html#ad0f09614599bd18f0430d2e3e3f2acf1", null ],
    [ "numLayers", "structktxTextureCreateInfo.html#a237cfa756c1b49169d769442c1c5492a", null ],
    [ "numLevels", "structktxTextureCreateInfo.html#a5bc01f6ce3a1082921cc307d80b4662c", null ],
    [ "pDfd", "structktxTextureCreateInfo.html#a0d39d6c4f004b3d1314d9684ffc3d8b4", null ],
    [ "vkFormat", "structktxTextureCreateInfo.html#aa1bc25cfcfdc8c0753a265753312ef65", null ]
];