var group__writer =
[
    [ "ktxTexture1_Create", "group__writer.html#ga761f4c933557aefa157900c076357c3e", null ],
    [ "ktxTexture1_SetImageFromMemory", "group__writer.html#gabeb1d5eb7ddcfd81405d5726a0b55ee6", null ],
    [ "ktxTexture1_SetImageFromStdioStream", "group__writer.html#ga9383755c2e02a9d1aa932e75c847829c", null ],
    [ "ktxTexture1_WriteKTX2ToMemory", "group__writer.html#gadbc6058afa50d4db3efd738822e6296a", null ],
    [ "ktxTexture1_WriteKTX2ToNamedFile", "group__writer.html#ga3009e876fb584268241a9ba7704e8557", null ],
    [ "ktxTexture1_WriteKTX2ToStdioStream", "group__writer.html#gac31b3532ed02d8cd4a7b517d4d2ab6ef", null ],
    [ "ktxTexture1_WriteToMemory", "group__writer.html#ga21904a85ada4ee322b87c309566ea4d1", null ],
    [ "ktxTexture1_WriteToNamedFile", "group__writer.html#gafe0d51799d92b69cdcbe30c98322340b", null ],
    [ "ktxTexture1_WriteToStdioStream", "group__writer.html#ga924da3380253c85fffca8c956e00d5c0", null ],
    [ "ktxTexture2_CompressBasis", "group__writer.html#ga405c44d6daf8ddf83dc805810bf4f989", null ],
    [ "ktxTexture2_CompressBasisEx", "group__writer.html#ga0e75755e3cc69964e57363dad82ffd77", null ],
    [ "ktxTexture2_Create", "group__writer.html#ga9ff411caf2251c100a581960deaf9bb7", null ],
    [ "ktxTexture2_CreateCopy", "group__writer.html#ga5fcfdc3d237419ab65287639e1f4aeb1", null ],
    [ "ktxTexture2_DeflateZstd", "group__writer.html#ga08d7222fbf845f25d883ed81c14c6430", null ],
    [ "ktxTexture2_SetImageFromMemory", "group__writer.html#ga01b13c38300016ce36c5b97bf4957d48", null ],
    [ "ktxTexture2_SetImageFromStdioStream", "group__writer.html#ga50a5ecf22586b26977da62cf19a78518", null ],
    [ "ktxTexture2_WriteToMemory", "group__writer.html#ga0113194b669b789a22d85b7ad992f117", null ],
    [ "ktxTexture2_WriteToNamedFile", "group__writer.html#ga745cee3857ade59e612c533d6faa8e2a", null ],
    [ "ktxTexture2_WriteToStdioStream", "group__writer.html#gadf2c2542471b7a94636216807cedd819", null ]
];