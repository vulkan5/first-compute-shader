var modules =
[
    [ "OpenGL Texture Image Loader", "group__ktx__glloader.html", "group__ktx__glloader" ],
    [ "Reader", "group__reader.html", "group__reader" ],
    [ "Vulkan Texture Image Loader", "group__ktx__vkloader.html", "group__ktx__vkloader" ],
    [ "Writer", "group__writer.html", "group__writer" ]
];