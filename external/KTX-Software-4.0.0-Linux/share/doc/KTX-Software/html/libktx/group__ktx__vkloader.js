var group__ktx__vkloader =
[
    [ "ktxTexture1_GetVkFormat", "group__ktx__vkloader.html#ga11fb504b749453140e04ca69711633d6", null ],
    [ "ktxTexture1_VkUpload", "group__ktx__vkloader.html#gac70d24bac4ac3b2586836439bbc6592a", null ],
    [ "ktxTexture1_VkUploadEx", "group__ktx__vkloader.html#gad7840c1eeb618fa1a09fc5a02c07ce87", null ],
    [ "ktxTexture2_GetVkFormat", "group__ktx__vkloader.html#ga7a9baaf3aa2e114c6f6e58fe68051815", null ],
    [ "ktxTexture2_VkUpload", "group__ktx__vkloader.html#gadf0fba44f518b61acd5d0ae86225ddee", null ],
    [ "ktxTexture2_VkUploadEx", "group__ktx__vkloader.html#ga265926439e3f9502f248684238e5cc48", null ],
    [ "ktxTexture_GetVkFormat", "group__ktx__vkloader.html#ga3ef2792fb0cd184636180ae0a540b872", null ],
    [ "ktxTexture_VkUpload", "group__ktx__vkloader.html#ga377a4a2c177956ea661549ee502d60da", null ],
    [ "ktxTexture_VkUploadEx", "group__ktx__vkloader.html#ga153164adbd7307ad1844c3e117faa325", null ],
    [ "ktxVulkanDeviceInfo_Construct", "group__ktx__vkloader.html#ga4bc9f0fa9af93d588276f54fe9a6ba50", null ],
    [ "ktxVulkanDeviceInfo_Create", "group__ktx__vkloader.html#ga82ac7e21e884652c519d9fe28ad5428c", null ],
    [ "ktxVulkanDeviceInfo_Destroy", "group__ktx__vkloader.html#gaaf633943fbf201fb620c0270c6150fa8", null ],
    [ "ktxVulkanDeviceInfo_Destruct", "group__ktx__vkloader.html#gae58928740420d1ed3f96fd4b0f2d897e", null ],
    [ "ktxVulkanTexture_Destruct", "group__ktx__vkloader.html#gaf620a84f6bc59bff03b521e55a654f19", null ]
];