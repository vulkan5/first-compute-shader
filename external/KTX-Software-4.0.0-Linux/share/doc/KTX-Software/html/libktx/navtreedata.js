/*
@licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2019 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "libktx - The KTX Library", "index.html", [
    [ "Introduction", "index.html", [
      [ "Usage Overview", "index.html#overview", [
        [ "Reading a KTX file for non-GL and non-Vulkan Use", "index.html#readktx", null ],
        [ "Creating a GL texture object from a KTX file.", "index.html#createGL", null ],
        [ "Creating a Vulkan image object from a KTX file.", "index.html#createVulkan", null ],
        [ "Extracting Metadata", "index.html#subsection", null ],
        [ "Writing a KTX or KTX2 file", "index.html#writektx", null ],
        [ "Modifying a KTX file", "index.html#modifyktx", null ],
        [ "Writing a BasisLZ/ETC1S-compressed Texture", "index.html#autotoc_md0", null ],
        [ "Transcoding a BasisLZ/ETC1S-compressed Texture", "index.html#autotoc_md1", null ]
      ] ]
    ] ],
    [ "LICENSE file for the KhronosGroup/KTX-Software project", "license.html", [
      [ "Special Cases", "license.html#autotoc_md2", null ]
    ] ],
    [ "Revision History", "libktx_history.html", [
      [ "Version 4.0", "libktx_history.html#v8", null ],
      [ "Version 3.0.1", "libktx_history.html#v7", null ],
      [ "Version 3.0", "libktx_history.html#v6", null ],
      [ "Version 2.0.2", "libktx_history.html#v5", null ],
      [ "Version 2.0.1", "libktx_history.html#v4", null ],
      [ "Version 2.0", "libktx_history.html#v3", null ],
      [ "Version 1.0.1", "libktx_history.html#v2", null ],
      [ "Version 1.0", "libktx_history.html#v1", null ]
    ] ],
    [ "Deprecated List", "deprecated.html", null ],
    [ "Modules", "modules.html", "modules" ],
    [ "Data Structures", "annotated.html", [
      [ "Data Structures", "annotated.html", "annotated_dup" ],
      [ "Data Fields", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ],
        [ "Typedefs", "functions_type.html", null ],
        [ "Enumerations", "functions_enum.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ],
      [ "Globals", "globals.html", [
        [ "All", "globals.html", null ],
        [ "Functions", "globals_func.html", null ],
        [ "Typedefs", "globals_type.html", null ],
        [ "Enumerations", "globals_enum.html", null ],
        [ "Enumerator", "globals_eval.html", null ],
        [ "Macros", "globals_defs.html", null ]
      ] ]
    ] ],
    [ "Examples", "examples.html", "examples" ]
  ] ]
];

var NAVTREEINDEX =
[
"annotated.html",
"ktx_8h.html#a7780d97ec4de6aa30c4726fb71e3116ca85733279b7d3e00ad9a6d498d6686645"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';