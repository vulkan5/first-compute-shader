var indexSectionsWithContent =
{
  0: "_bcdeghiklmnopqrstuvwxyz",
  1: "k",
  2: "ks",
  3: "k",
  4: "_bcdeghiklmnopqstuvwxyz",
  5: "kp",
  6: "k",
  7: "k",
  8: "k",
  9: "orvw",
  10: "dilr"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "defines",
  9: "groups",
  10: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros",
  9: "Modules",
  10: "Pages"
};

