var searchData=
[
  ['image_416',['image',['../structktxVulkanTexture.html#a8e1c1e2f71c9921d1cb47d50a6e7a8cb',1,'ktxVulkanTexture']]],
  ['imageformat_417',['imageFormat',['../structktxVulkanTexture.html#a51a2976a43d1c449904208ab52ee217f',1,'ktxVulkanTexture']]],
  ['imagelayout_418',['imageLayout',['../structktxVulkanTexture.html#ad86553423ca7b214849b6a26bd1c4010',1,'ktxVulkanTexture']]],
  ['inputswizzle_419',['inputSwizzle',['../structktxBasisParams.html#a2b3befb6617cde21ca3e5b222257c66c',1,'ktxBasisParams']]],
  ['isarray_420',['isArray',['../structktxTexture.html#ad18302edd81e0368b93d7fdd517813e8',1,'ktxTexture::isArray()'],['../structktxTextureCreateInfo.html#a7f6d6379fcb54b95769000b3919a659b',1,'ktxTextureCreateInfo::isArray()']]],
  ['iscubemap_421',['isCubemap',['../structktxTexture.html#a4e9ae7f75a7e1f05ca4f9eacba84db82',1,'ktxTexture']]]
];
