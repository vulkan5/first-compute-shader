var searchData=
[
  ['pallocator_258',['pAllocator',['../structktxVulkanDeviceInfo.html#ab1067ca1dda7c8aa2da946e867622c1e',1,'ktxVulkanDeviceInfo']]],
  ['pdata_259',['pData',['../structktxTexture.html#a979bf5b23162095d6ca6ba720b134f21',1,'ktxTexture']]],
  ['pdfd_260',['pDfd',['../structktxTextureCreateInfo.html#a0d39d6c4f004b3d1314d9684ffc3d8b4',1,'ktxTextureCreateInfo']]],
  ['pfnktxitercb_261',['PFNKTXITERCB',['../structktxTexture.html#ac17c25cbbbd8fab648b55eced7111b96',1,'ktxTexture']]],
  ['physicaldevice_262',['physicalDevice',['../structktxVulkanDeviceInfo.html#ac59a6dca1cb19c69dc83eb2271088a0a',1,'ktxVulkanDeviceInfo']]],
  ['preswizzle_263',['preSwizzle',['../structktxBasisParams.html#ab6e76b713d00fe3784d46eb768262616',1,'ktxBasisParams']]]
];
