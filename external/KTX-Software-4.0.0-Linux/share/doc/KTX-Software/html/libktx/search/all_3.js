var searchData=
[
  ['datasize_9',['dataSize',['../structktxTexture.html#af42a643a2d435e18f54dcde6b10f2a66',1,'ktxTexture']]],
  ['deprecated_20list_10',['Deprecated List',['../deprecated.html',1,'']]],
  ['depth_11',['depth',['../structktxVulkanTexture.html#af990e0fd73d55500f48d616a0f35a470',1,'ktxVulkanTexture']]],
  ['device_12',['device',['../structktxVulkanDeviceInfo.html#a6021332b7544982e536f1b88d2758999',1,'ktxVulkanDeviceInfo']]],
  ['devicememory_13',['deviceMemory',['../structktxVulkanTexture.html#ae78e0952193debfc52b76f2978cca58c',1,'ktxVulkanTexture']]],
  ['devicememoryproperties_14',['deviceMemoryProperties',['../structktxVulkanDeviceInfo.html#afa5fb09215087b5ff27022eeab52c7e8',1,'ktxVulkanDeviceInfo']]]
];
