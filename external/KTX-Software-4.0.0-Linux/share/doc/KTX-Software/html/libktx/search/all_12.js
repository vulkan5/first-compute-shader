var searchData=
[
  ['uastc_273',['uastc',['../structktxBasisParams.html#a7f9092685bb7f21656c08ada636dded6',1,'ktxBasisParams']]],
  ['uastcflags_274',['uastcFlags',['../structktxBasisParams.html#a3cae71fd75deacacb658f52917df6db1',1,'ktxBasisParams']]],
  ['uastcrdo_275',['uastcRDO',['../structktxBasisParams.html#a9d6a6d05cbb6b10bf72ee9e89759581e',1,'ktxBasisParams']]],
  ['uastcrdodictsize_276',['uastcRDODictSize',['../structktxBasisParams.html#a7022e40fb4d350016de552cca296aedc',1,'ktxBasisParams']]],
  ['uastcrdodontfavorsimplermodes_277',['uastcRDODontFavorSimplerModes',['../structktxBasisParams.html#ae98de348622faacd1d5f5d7bb5ace7b5',1,'ktxBasisParams']]],
  ['uastcrdomaxsmoothblockerrorscale_278',['uastcRDOMaxSmoothBlockErrorScale',['../structktxBasisParams.html#a21b9198210b22ff345299b9c1b48bfa1',1,'ktxBasisParams']]],
  ['uastcrdomaxsmoothblockstddev_279',['uastcRDOMaxSmoothBlockStdDev',['../structktxBasisParams.html#a88590bbf8d6a5edd0afce3356b49c296',1,'ktxBasisParams']]],
  ['uastcrdonomultithreading_280',['uastcRDONoMultithreading',['../structktxBasisParams.html#ac1d4f91fb9c5a420f76fa7c194c8d04f',1,'ktxBasisParams']]],
  ['uastcrdoqualityscalar_281',['uastcRDOQualityScalar',['../structktxBasisParams.html#ad2bbffd908482442da4efcea86af1d7d',1,'ktxBasisParams']]]
];
