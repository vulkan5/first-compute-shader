/*
@licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2019 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Khronos Texture Software", "index.html", [
    [ "Abstract", "abstract.html", null ],
    [ "libktx", "libktx_main.html", null ],
    [ "KTX Tools", "ktxtools.html", [
      [ "ktx2check", "ktxtools.html#autotoc_md2", null ],
      [ "ktx2ktx2", "ktxtools.html#autotoc_md3", null ],
      [ "ktxinfo", "ktxtools.html#autotoc_md4", null ],
      [ "ktxsc", "ktxtools.html#autotoc_md5", null ],
      [ "toktx", "ktxtools.html#autotoc_md6", null ]
    ] ],
    [ "Authors", "authors.html", null ],
    [ "To Do List", "todo.html", [
      [ "library", "todo.html#autotoc_md9", null ],
      [ "library testing", "todo.html#autotoc_md10", null ],
      [ "toktx", "todo.html#autotoc_md11", null ]
    ] ],
    [ "Javascript Bindings", "js_bindings.html", "js_bindings" ]
  ] ]
];

var NAVTREEINDEX =
[
"abstract.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';