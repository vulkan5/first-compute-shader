
#ifndef VKM_SWAPCHAIN_HPP
#define VKM_SWAPCHAIN_HPP

#define VK_CHECK( result ) do { \
   assert(result == VK_SUCCESS); \
} while ( 0 ) \

#include <vulkan/vulkan.h>
#include <GLFW/glfw3.h>
#include <assert.h>

#include "vkmPhysicalDevice.hpp"
#include "vkmDevice.hpp"
#include "vkmTools.hpp"

class vkmSwapchain
{
private:
    vkmPhysicalDevice& physicalDevice;
    vkmDevice& device;

    uint32_t numImages;
    VkFormat imageFormat;
    VkExtent2D imageExtent;
    std::vector<VkImage> images;
    std::vector<VkImageView> imageViews;

public:
    VkSwapchainKHR vk_swapchain;
    uint32_t currentImageIdx;

    vkmSwapchain(vkmPhysicalDevice& _physicalDevice, vkmDevice& _device)
        : physicalDevice(_physicalDevice)
        , device(_device)
    {}

    ~vkmSwapchain()
    {
        for (size_t i = 0; i < images.size(); ++i)
        {
            vkDestroyImageView(device.vk_device, imageViews[i], nullptr);
        }

        vkDestroySwapchainKHR(device.vk_device, vk_swapchain, nullptr);
    }

    void init(const uint32_t imageCount, const VkExtent2D extent)
    {
        VkSurfaceCapabilitiesKHR surfaceCapabilities;
        VK_CHECK(vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physicalDevice.vk_physicalDevice, physicalDevice.vk_surfaceKHR, &surfaceCapabilities));
        VkSwapchainCreateInfoKHR swapchainCreateInfo = { VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR };
        swapchainCreateInfo.surface = physicalDevice.vk_surfaceKHR;

        const VkFormat requested_surface_format = VK_FORMAT_A1R5G5B5_UNORM_PACK16;

        //** Image Count
        {
            assert(imageCount > 0 && "Invalid requested image count for swapchain!");

            // If the minImageCount is 0, then there is not a limit on the number of images the swapchain
            // can support (ignoring memory constraints). See the Vulkan Spec for more information.
            if (surfaceCapabilities.maxImageCount == 0)
            {
                if (imageCount >= surfaceCapabilities.minImageCount)
                {
                    swapchainCreateInfo.minImageCount = imageCount;
                }
                else
                {
                    printf("Failed to create Swapchain. The requested number of images %u does not meet the minimum requirement of %u.\n", imageCount, surfaceCapabilities.minImageCount);
                    exit(EXIT_FAILURE);
                }
            }
            else if (imageCount >= surfaceCapabilities.minImageCount &&
                     imageCount <= surfaceCapabilities.maxImageCount)
            {
                swapchainCreateInfo.minImageCount = imageCount;
            }
            else
            {
                printf("The number of requested Swapchain images %u is not supported. Min: %u\t Max: %u\n", imageCount, surfaceCapabilities.minImageCount, surfaceCapabilities.maxImageCount);
                exit(EXIT_FAILURE);
            }
        }

        //** Image Format
        {
            uint32_t numSupportedSurfaceFormats = 0;
            VK_CHECK(vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice.vk_physicalDevice, physicalDevice.vk_surfaceKHR, &numSupportedSurfaceFormats, nullptr));
            VkSurfaceFormatKHR* supportedSurfaceFormats = new VkSurfaceFormatKHR[numSupportedSurfaceFormats];
            VK_CHECK(vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice.vk_physicalDevice, physicalDevice.vk_surfaceKHR, &numSupportedSurfaceFormats, supportedSurfaceFormats));

            bool requestedFormatFound = false;
            for (uint32_t i = 0; i < numSupportedSurfaceFormats; ++i)
            {
                if (supportedSurfaceFormats[i].format == requested_surface_format)
                {
                    swapchainCreateInfo.imageFormat = supportedSurfaceFormats[i].format;
                    swapchainCreateInfo.imageColorSpace = supportedSurfaceFormats[i].colorSpace;
                    requestedFormatFound = true;
                    break;
                }
            }

            if (!requestedFormatFound)
            {
                swapchainCreateInfo.imageFormat = supportedSurfaceFormats[0].format;
                swapchainCreateInfo.imageColorSpace = supportedSurfaceFormats[0].colorSpace;
                // printf("WARNING - Requested format %u is not avaliable! Defaulting to first avaliable.\n", (unsigned int)requested_surface_format);
            }

            imageFormat = swapchainCreateInfo.imageFormat;
            std::cout << "Swapchain Image Format: " << convert(swapchainCreateInfo.imageFormat) << '\n';
            delete [] supportedSurfaceFormats;
        }

        //** Extent
        {
            // The Vulkan Spec states that if the current width/height is 0xFFFFFFFF, then the surface size
            // will be deteremined by the extent specified in the VkSwapchainCreateInfoKHR.
            if (surfaceCapabilities.currentExtent.width != (uint32_t)-1)
            {
                swapchainCreateInfo.imageExtent = extent;
            }
            else
            {
                swapchainCreateInfo.imageExtent = surfaceCapabilities.currentExtent;
            }

            imageExtent = swapchainCreateInfo.imageExtent;
        }

        swapchainCreateInfo.imageArrayLayers = 1;
        swapchainCreateInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
        swapchainCreateInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
        swapchainCreateInfo.queueFamilyIndexCount = 0;
        swapchainCreateInfo.pQueueFamilyIndices = nullptr;

        //** Pre Transform
        {
            if (surfaceCapabilities.supportedTransforms & VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR)
            {
                swapchainCreateInfo.preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
            }
            else
            {
                swapchainCreateInfo.preTransform = surfaceCapabilities.currentTransform;
                printf("WARNING - Swapchain pretransform is not IDENTITIY_BIT_KHR!\n");
            }
        }

        //** Composite Alpha
        {
            // Determine the composite alpha format the application needs.
        	// Find a supported composite alpha format (not all devices support alpha opaque),
            // but we prefer it.
        	// Simply select the first composite alpha format available
            // Used for blending with other windows in the system
        	VkCompositeAlphaFlagBitsKHR compositeAlphaFlags[4] = {
        		VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
        		VK_COMPOSITE_ALPHA_PRE_MULTIPLIED_BIT_KHR,
        		VK_COMPOSITE_ALPHA_POST_MULTIPLIED_BIT_KHR,
        		VK_COMPOSITE_ALPHA_INHERIT_BIT_KHR,
        	};
            for (size_t i = 0; i < 4; ++i)
            {
        		if (surfaceCapabilities.supportedCompositeAlpha & compositeAlphaFlags[i]) 
                {
                    swapchainCreateInfo.compositeAlpha = compositeAlphaFlags[i];
                    break;
        		};
            }
        }

        //** Present Mode
        {
            uint32_t numSupportedPresentModes = 0;
            VK_CHECK(vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice.vk_physicalDevice, physicalDevice.vk_surfaceKHR, &numSupportedPresentModes, nullptr));
            VkPresentModeKHR* supportedPresentModes = new VkPresentModeKHR[numSupportedPresentModes];
            VK_CHECK(vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice.vk_physicalDevice, physicalDevice.vk_surfaceKHR, &numSupportedPresentModes, supportedPresentModes));

            // Determine the present mode the application needs.
            // Try to use mailbox, it is the lowest latency non-tearing present mode
            // All devices support FIFO (this mode waits for the vertical blank or v-sync)
            swapchainCreateInfo.presentMode = VK_PRESENT_MODE_FIFO_KHR;
            for (uint32_t i = 0; i < numSupportedPresentModes; ++i)
            {
                if (supportedPresentModes[i] == VK_PRESENT_MODE_MAILBOX_KHR)
                {
                    swapchainCreateInfo.presentMode = VK_PRESENT_MODE_MAILBOX_KHR;
                    break;
                }
            }

            delete [] supportedPresentModes;
            std::cout << "Swapchain present mode: " << convert(swapchainCreateInfo.presentMode) << '\n';
        }

        //** Creating Swapchain
        swapchainCreateInfo.clipped = VK_TRUE;
        swapchainCreateInfo.oldSwapchain = VK_NULL_HANDLE;
        VK_CHECK(vkCreateSwapchainKHR(device.vk_device, &swapchainCreateInfo, nullptr, &vk_swapchain));

        //** Get Images/ImageViews
        {
            uint32_t numSwapchainImages = 0;
            VK_CHECK(vkGetSwapchainImagesKHR(device.vk_device, vk_swapchain, &numSwapchainImages, nullptr));
            images.reserve(numSwapchainImages);
            imageViews.reserve(numSwapchainImages);
            VK_CHECK(vkGetSwapchainImagesKHR(device.vk_device, vk_swapchain, &numSwapchainImages, images.data()));

            VkImageViewCreateInfo imageViewCreateInfo {
                .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
                .viewType = VK_IMAGE_VIEW_TYPE_2D,
                .format = swapchainCreateInfo.imageFormat,
                .components = {
                    .r = VK_COMPONENT_SWIZZLE_IDENTITY,
                    .g = VK_COMPONENT_SWIZZLE_IDENTITY,
                    .b = VK_COMPONENT_SWIZZLE_IDENTITY,
                    .a = VK_COMPONENT_SWIZZLE_IDENTITY },
                .subresourceRange = {
                    .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
                    .baseMipLevel = 0,
                    .levelCount = 1,
                    .baseArrayLayer = 0,
                    .layerCount = 1 },
            };

            for (size_t i = 0; i < numSwapchainImages; ++i)
            {
                imageViewCreateInfo.image = images[i];
                VK_CHECK(vkCreateImageView(device.vk_device, &imageViewCreateInfo, nullptr, &imageViews[i]));
            }

            std::cout << "Swapchain Image Count: " << numSwapchainImages << '\n';
            numImages = numSwapchainImages;
        }
    }

    const uint32_t getNumImages() const { return numImages; } 
    const VkFormat getImageFormat() const { return imageFormat; }
    const VkImageView getImageView(const int idx) const { return imageViews[idx]; }
    const VkExtent2D getExtent() const { return imageExtent; }
};

#endif // VKM_SWAPCHAIN_HPP