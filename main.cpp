#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <cmath>
#include <vector>
#include <iostream>
#include <stdlib.h>
#include <cstring>
#include <array>

#include <vulkan/vulkan.h>
#include <GLFW/glfw3.h>
#include "external/KTX-Software-4.0.0-Linux/include/ktx.h"

#include "vkmInstance.hpp"
#include "vkmPhysicalDevice.hpp"
#include "vkmDevice.hpp"
#include "vkmSwapchain.hpp"
#include "vkmRenderPass.hpp"

static constexpr int WIN_WIDTH  = 512;
static constexpr int WIN_HEIGHT = 512;
static constexpr uint32_t NUM_SWAPCHAIN_IMAGES = 2;
static constexpr uint32_t NUM_FRAMES_IN_FLIGHT = 2;

struct VertexData
{
    float pos[2];
    float uv[2];
};

std::array<VertexData, 6> vertexData = {{
    { .pos = {  1,  1 }, .uv = { 0, 0 } },
    { .pos = { -1,  1 }, .uv = { 1, 0 } },
    { .pos = { -1, -1 }, .uv = { 1, 1 } },

    { .pos = {  1,  1 }, .uv = { 0, 0 } },
    { .pos = { -1, -1 }, .uv = { 1, 1 } },
    { .pos = {  1, -1 }, .uv = { 0, 1 } },
}};

class Scene
{
private:
public:
};

class vkmPipeline
{
private:
public:
    VkPipelineLayout vk_pipelineLayout;
    VkPipeline vk_pipeline;
};

class App
{
private:
    vkmPhysicalDevice& physicalDevice;
    vkmDevice& device;

    vkmSwapchain swapchain;
    vkmRenderPass renderPass;
    uint32_t currentFrameIdx;
    std::array<VkFramebuffer, NUM_SWAPCHAIN_IMAGES> vk_framebuffers;

    VkCommandPool renderCommandPool;
    VkCommandBuffer renderCommandBuffer;

    struct DescriptorManager
    {
        VkDescriptorSetLayout vk_descriptorSetLayout;
        VkDescriptorPool vk_descriptorPool;
        VkDescriptorSet vk_descriptorSet;
    } descriptorManager;

    enum class Pipeline
    {
       DEFAULT = 0,
       NPIPELINES 
    };

    std::array<vkmPipeline, static_cast<size_t>(Pipeline::NPIPELINES)> pipelines; 

    // Move this into scene class

    VkSampler sampler;
    vkmImage image;
    VkImageView imageView;
    vkmBuffer vertexBuffer;

    void initDefaults()
    {
        swapchain.init(NUM_SWAPCHAIN_IMAGES, { WIN_WIDTH, WIN_HEIGHT });
        assert(swapchain.getNumImages() == NUM_SWAPCHAIN_IMAGES);

        renderPass.init(swapchain);

        VkFramebufferCreateInfo framebufferCreateInfo {
            .sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
            .renderPass = renderPass.vk_renderPass,
            .attachmentCount = 1,
            .width = swapchain.getExtent().width,
            .height = swapchain.getExtent().height,
            .layers = 1
        };

        for (size_t i = 0; i < NUM_FRAMES_IN_FLIGHT; ++i)
        {
            VkImageView attachments[1] = {
                swapchain.getImageView(i)
            };

            framebufferCreateInfo.pAttachments = attachments;
   
            VK_CHECK(vkCreateFramebuffer(device.vk_device, &framebufferCreateInfo, nullptr, &vk_framebuffers[i]));
        }

        //** Allocating Graphics Command Pool and Buffers
        {
            const VkCommandPoolCreateInfo commandPoolCreateInfo {
                .sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
                .queueFamilyIndex = physicalDevice.graphicsQueueFamilyIndex
            };

            VK_CHECK(vkCreateCommandPool(device.vk_device, &commandPoolCreateInfo, nullptr, &renderCommandPool));

            const VkCommandBufferAllocateInfo commandBufferAllocateInfo {
                .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
                .commandPool = renderCommandPool,
                .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
                .commandBufferCount = 1,
            };

            VK_CHECK(vkAllocateCommandBuffers(device.vk_device, &commandBufferAllocateInfo, &renderCommandBuffer));
        }

    }

    void initPipelines()
    {
        //** Layout
        {
            const VkDescriptorSetLayoutBinding descriptorSetLayoutBinding[1] {
                {
                    .binding = 0,
                    .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                    .descriptorCount = 1,
                    .stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
                    .pImmutableSamplers = nullptr,
                }
            };

            const VkDescriptorSetLayoutCreateInfo descriptorSetLayoutCreateInfo[1] {
                {
                    .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
                    .bindingCount = 1,
                    .pBindings = descriptorSetLayoutBinding,
                }
            };

            VK_CHECK(vkCreateDescriptorSetLayout(device.vk_device, descriptorSetLayoutCreateInfo, nullptr, &descriptorManager.vk_descriptorSetLayout));
        }

        //** Pool
        {
            const VkDescriptorPoolSize descriptorPoolSize {
                .type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                .descriptorCount = 1
            };

            const VkDescriptorPoolCreateInfo descriptorPoolCreateInfo {
                .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
                .maxSets = 1,
                .poolSizeCount = 1,
                .pPoolSizes = &descriptorPoolSize,
            };

            VK_CHECK(vkCreateDescriptorPool(device.vk_device, &descriptorPoolCreateInfo, nullptr, &descriptorManager.vk_descriptorPool));
        }
      
        //** Set
        {
            const VkDescriptorSetAllocateInfo descriptorSetAllocateInfo {
                .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
                .pNext = nullptr,
                .descriptorPool = descriptorManager.vk_descriptorPool,
                .descriptorSetCount = 1,
                .pSetLayouts = &descriptorManager.vk_descriptorSetLayout,
            };

            VK_CHECK(vkAllocateDescriptorSets(device.vk_device, &descriptorSetAllocateInfo, &descriptorManager.vk_descriptorSet));
        }

        //** Default Graphics Pipeline 
        {
            //** Shader Stage
            const VkPipelineShaderStageCreateInfo pipelineShaderStageCreateInfo[2] {
                {
                    .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
                    .stage = VK_SHADER_STAGE_VERTEX_BIT,
                    .module = device.createShaderModule("../shaders/default-vert.spv"),
                    .pName = "main",
                    .pSpecializationInfo = nullptr,
                },
                {
                    .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
                    .stage = VK_SHADER_STAGE_FRAGMENT_BIT,
                    .module = device.createShaderModule("../shaders/default-frag.spv"),
                    .pName = "main",
                    .pSpecializationInfo = nullptr,
                }
            };

            //** Input State
            const std::array<VkVertexInputBindingDescription, 1> vertexInputBindingDescription {{
                {
                    .binding = 0,
                    .stride = sizeof(float) * 4,
                    .inputRate = VK_VERTEX_INPUT_RATE_VERTEX
                }
            }};

            const std::array<VkVertexInputAttributeDescription, 2> vertexInputAttributeDescription {{
                {
                    .location = 0,
                    .binding = 0,
                    .format = VK_FORMAT_R32G32_SFLOAT,
                    .offset = offsetof(VertexData, pos),
                },
                {
                    .location = 1,
                    .binding = 0,
                    .format = VK_FORMAT_R32G32_SFLOAT,
                    .offset = offsetof(VertexData, uv),
                },
            }};

            const VkPipelineVertexInputStateCreateInfo pipelineVertexInputStateCreateInfo {
                .sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
                .vertexBindingDescriptionCount = vertexInputBindingDescription.size(),
                .pVertexBindingDescriptions = vertexInputBindingDescription.data(),
                .vertexAttributeDescriptionCount = vertexInputAttributeDescription.size(),
                .pVertexAttributeDescriptions = vertexInputAttributeDescription.data()
            };

            //** Input Assembly
            const VkPipelineInputAssemblyStateCreateInfo pipelineInputAssemblyStateCreateInfo {
                .sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
                .topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
                .primitiveRestartEnable = VK_FALSE,
            };

            //** Tessellation State
            const VkPipelineTessellationStateCreateInfo ci_pipeline_tesselation_state = { VK_STRUCTURE_TYPE_PIPELINE_TESSELLATION_STATE_CREATE_INFO };

            //** Viewport State
            const VkViewport viewport {
                .x = 0,
                .y = 0,
                .width = static_cast<float>(swapchain.getExtent().width),
                .height = static_cast<float>(swapchain.getExtent().height),
                .minDepth = 0.0f,
                .maxDepth = 1.0f,
            };

            const VkRect2D scissorRect {
                .offset = {.x = 0, .y = 0},
                .extent = swapchain.getExtent()
            };

            const VkPipelineViewportStateCreateInfo pipelineViewportStateCreateInfo {
                .sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
                .viewportCount = 1,
                .pViewports = &viewport,
                .scissorCount = 1,
                .pScissors = &scissorRect,
            };

            //** Rasterization State
            const VkPipelineRasterizationStateCreateInfo pipelineRasterizationStateCreateInfo {
                .sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
                .depthClampEnable = VK_FALSE,
                .rasterizerDiscardEnable = VK_FALSE,
                .polygonMode = VK_POLYGON_MODE_FILL,
                .cullMode = VK_CULL_MODE_NONE,
                .frontFace = VK_FRONT_FACE_CLOCKWISE,
                .depthBiasEnable = VK_FALSE,
                .depthBiasConstantFactor = 0.f,
                .depthBiasClamp = 0.f,
                .depthBiasSlopeFactor = 0.f,
                .lineWidth = 1.f,
            };

            //** MultiSample State
            const VkPipelineMultisampleStateCreateInfo pipelineMulisampleStateCreateInfo {
                .sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
                .rasterizationSamples = VK_SAMPLE_COUNT_1_BIT,
                .sampleShadingEnable = VK_FALSE,
                .minSampleShading = 0,
                .pSampleMask = nullptr,
                .alphaToCoverageEnable = VK_FALSE,
                .alphaToOneEnable = VK_FALSE,
            };

            //** Depth/Stencil State
            const VkStencilOpState frontStencilOpState {
                .failOp = VK_STENCIL_OP_KEEP,
                .passOp = VK_STENCIL_OP_KEEP,
                .depthFailOp = VK_STENCIL_OP_KEEP,
                .compareOp = VK_COMPARE_OP_NEVER,
                .compareMask = 0x0,
                .writeMask = 0x0,
                .reference = 0x0,
            };

            const VkStencilOpState backStencilOpState {
                .failOp = VK_STENCIL_OP_KEEP,
                .passOp = VK_STENCIL_OP_KEEP,
                .depthFailOp = VK_STENCIL_OP_KEEP,
                .compareOp = VK_COMPARE_OP_NEVER,
                .compareMask = 0x0,
                .writeMask = 0x0,
                .reference = 0x0,
            };

            const VkPipelineDepthStencilStateCreateInfo pipelineDepthStencilStateCreateInfo {
                .sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
                .depthTestEnable = VK_FALSE,
                .depthWriteEnable = VK_FALSE,
                .depthCompareOp = VK_COMPARE_OP_LESS,
                .depthBoundsTestEnable = VK_FALSE,
                .stencilTestEnable = VK_FALSE,
                .front = frontStencilOpState,
                .back = backStencilOpState,
                .minDepthBounds = 0.f,
                .maxDepthBounds = 1.f,
            };

            //** Blend State
            const VkPipelineColorBlendAttachmentState pipelineColorBlendState {
                .blendEnable = VK_FALSE,
                .srcColorBlendFactor = VK_BLEND_FACTOR_ZERO,
                .dstColorBlendFactor = VK_BLEND_FACTOR_ZERO,
                .colorBlendOp = VK_BLEND_OP_ADD,
                .srcAlphaBlendFactor = VK_BLEND_FACTOR_ZERO,
                .dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO,
                .alphaBlendOp = VK_BLEND_OP_ADD,
                .colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT,
            };
            
            const VkPipelineColorBlendStateCreateInfo pipelineColorBlendStateCreateInfo {
                .sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
                .logicOpEnable = VK_FALSE,
                .logicOp = VK_LOGIC_OP_COPY,
                .attachmentCount = 1,
                .pAttachments = &pipelineColorBlendState,
                .blendConstants = { 0, 0, 0, 0 }
            };

            //** Dynamic State
            const VkPipelineDynamicStateCreateInfo pipelineDynamicStateCreateInfo {
                .sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO,
                .dynamicStateCount = 0,
                .pDynamicStates = nullptr, // TODO - set this to viewportr and scissor for window resizing
            };

            //** Layout
            const VkPipelineLayoutCreateInfo pipelineLayoutCreateInfo {
                .sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
                .setLayoutCount = 1,
                .pSetLayouts = &descriptorManager.vk_descriptorSetLayout,
                .pushConstantRangeCount = 0,
                .pPushConstantRanges = nullptr,
            };
            vkmPipeline& pipeline = pipelines[static_cast<size_t>(Pipeline::DEFAULT)];

            VK_CHECK(vkCreatePipelineLayout(device.vk_device, &pipelineLayoutCreateInfo, nullptr, &pipeline.vk_pipelineLayout));

            //** Graphics Pipeline Creation
            const VkGraphicsPipelineCreateInfo graphicsPipelineCreateInfo {
                .sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
                .stageCount = 2,
                .pStages = pipelineShaderStageCreateInfo,
                .pVertexInputState = &pipelineVertexInputStateCreateInfo,
                .pInputAssemblyState = &pipelineInputAssemblyStateCreateInfo,
                // .pTessellationState = &ci_pipeline_tesselation_state,
                .pViewportState = &pipelineViewportStateCreateInfo,
                .pRasterizationState = &pipelineRasterizationStateCreateInfo,
                .pMultisampleState = &pipelineMulisampleStateCreateInfo,
                .pDepthStencilState = &pipelineDepthStencilStateCreateInfo,
                .pColorBlendState = &pipelineColorBlendStateCreateInfo,
                .pDynamicState = &pipelineDynamicStateCreateInfo,
                .layout = pipeline.vk_pipelineLayout,
                .renderPass = renderPass.vk_renderPass,
                .subpass = 0,
                .basePipelineHandle = VK_NULL_HANDLE,
                .basePipelineIndex = 0,
            }; 

            VK_CHECK(vkCreateGraphicsPipelines(device.vk_device, VK_NULL_HANDLE, 1, &graphicsPipelineCreateInfo, nullptr, &pipeline.vk_pipeline));

            vkDestroyShaderModule(device.vk_device, pipelineShaderStageCreateInfo[0].module, nullptr);
            vkDestroyShaderModule(device.vk_device, pipelineShaderStageCreateInfo[1].module, nullptr);
        }
    }

public:
    App(vkmPhysicalDevice& _physicalDevice, vkmDevice& _device, GLFWwindow* glfwWindow)
        : physicalDevice(_physicalDevice)
        , device(_device)
        , swapchain(_physicalDevice, _device)
        , renderPass(_device)
        , currentFrameIdx(0)
    {

        initDefaults();

        initPipelines();

        initScene();


        //** Init Code which stays the same from frame to frame
        {

        }

        VkClearValue clearValue {
            .color = { 0.22f, 0.22f, 0.22f, 1.0f }
        };

        VkRenderPassBeginInfo renderPassBeginInfo {
            .sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
            .renderPass = renderPass.vk_renderPass,
            .renderArea = {
                .offset = { .x = 0, .y = 0 },
                .extent = swapchain.getExtent() },
            .clearValueCount = 1,
            .pClearValues = &clearValue,
        };

        const VkCommandBufferBeginInfo renderCommandBufferBeginInfo {
            .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
        };

        vkmPipeline defaultPipeline = pipelines[static_cast<size_t>(Pipeline::DEFAULT)];

        VkFence acquireFence;
        VkFenceCreateInfo fenceCreateInfo { .sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO };
        VK_CHECK(vkCreateFence(device.vk_device, &fenceCreateInfo, nullptr, &acquireFence));

        while (!glfwWindowShouldClose(glfwWindow))
        {
            glfwPollEvents();

            // acquire
            VK_CHECK(vkAcquireNextImageKHR(device.vk_device, swapchain.vk_swapchain, UINT64_MAX, VK_NULL_HANDLE, acquireFence, &swapchain.currentImageIdx));
            VK_CHECK(vkWaitForFences(device.vk_device, 1, &acquireFence, VK_TRUE, UINT64_MAX));
            VK_CHECK(vkResetFences(device.vk_device, 1, &acquireFence));

            // cull

            // record
            {
                vkResetCommandPool(device.vk_device, renderCommandPool, 0x0);

                renderPassBeginInfo.framebuffer = vk_framebuffers[currentFrameIdx];

                VK_CHECK(vkBeginCommandBuffer(renderCommandBuffer, &renderCommandBufferBeginInfo));

                    vkCmdBeginRenderPass(renderCommandBuffer, &renderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);

                    vkCmdBindPipeline(renderCommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, defaultPipeline.vk_pipeline);
                    vkCmdBindDescriptorSets(renderCommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, defaultPipeline.vk_pipelineLayout, 0, 1, &descriptorManager.vk_descriptorSet, 0, nullptr);

                    const VkDeviceSize offset = 0;
                    vkCmdBindVertexBuffers(renderCommandBuffer, 0, 1, &vertexBuffer.vk_buffer, &offset);

                    vkCmdDraw(renderCommandBuffer, vertexData.size(), 1, 0, 0);

                    vkCmdEndRenderPass(renderCommandBuffer);

                VK_CHECK(vkEndCommandBuffer(renderCommandBuffer));
            }

            // submit
            {
                const VkSubmitInfo renderSubmitInfo {
                    .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
                    .waitSemaphoreCount = 0,
                    .pWaitSemaphores = nullptr,
                    .pWaitDstStageMask = nullptr,
                    .commandBufferCount = 1,
                    .pCommandBuffers = &renderCommandBuffer,
                    .signalSemaphoreCount = 0,
                    .pSignalSemaphores = nullptr,
                };

                VK_CHECK(vkQueueSubmit(device.getGraphicsQueue(), 1, &renderSubmitInfo, VK_NULL_HANDLE));

                VK_CHECK(vkDeviceWaitIdle(device.vk_device));

                //*** Present (wait for graphics work to complete)
                const VkPresentInfoKHR presentInfoKHR {
                    .sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
                    .waitSemaphoreCount = 0,
                    .pWaitSemaphores = nullptr,
                    .swapchainCount = 1,
                    .pSwapchains = &swapchain.vk_swapchain,
                    .pImageIndices = &swapchain.currentImageIdx,
                    .pResults = nullptr,
                };
                VK_CHECK(vkQueuePresentKHR(device.getGraphicsQueue(), &presentInfoKHR));

                VK_CHECK(vkDeviceWaitIdle(device.vk_device));
            }

            currentFrameIdx = (currentFrameIdx + 1) % NUM_FRAMES_IN_FLIGHT;
        }
    }

    ~App()
    {
        destroyScene();

        vkFreeCommandBuffers(device.vk_device, renderCommandPool, 1, &renderCommandBuffer);
        vkDestroyCommandPool(device.vk_device, renderCommandPool, nullptr);

        vkFreeDescriptorSets(device.vk_device, descriptorManager.vk_descriptorPool, 1, &descriptorManager.vk_descriptorSet);
        vkDestroyDescriptorPool(device.vk_device, descriptorManager.vk_descriptorPool, nullptr);
        vkDestroyDescriptorSetLayout(device.vk_device, descriptorManager.vk_descriptorSetLayout, nullptr);

        for (size_t i = 0; i < vk_framebuffers.size(); ++i)
        {
            vkDestroyFramebuffer(device.vk_device, vk_framebuffers[i], nullptr);
        }
    }

private:
    //! try specifiying different format
    //! try specifying linear vs optimal when doing sub-copies
    void initScene()
    {

        // Upload Texture Data
        {
            // Using Khronos texture format
            const char* filename = "/home/mica/Playground/C++/Vk-Test/textures/metalplate01_rgba.ktx";
            const VkFormat format = VK_FORMAT_R8G8B8A8_SNORM;

            ktxTexture* ktx_texture;
            KTX_error_code result; 
            result = ktxTexture_CreateFromNamedFile(filename, KTX_TEXTURE_CREATE_LOAD_IMAGE_DATA_BIT, &ktx_texture);
            if (ktx_texture == nullptr)
            {
                printf("Failed to load texture file %s\n", filename);
                exit(EXIT_FAILURE); 
            }

            // Put Texture Data in Staging Buffer

            vkmBuffer stagingBuffer;

            const VkBufferCreateInfo bufferCreateInfo = {
                .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
                .size  = static_cast<VkDeviceSize>(ktx_texture->dataSize),
                .usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
                .queueFamilyIndexCount = 1,
                .pQueueFamilyIndices = &physicalDevice.graphicsQueueFamilyIndex
            };

            device.createBuffer(bufferCreateInfo, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, stagingBuffer);

            uint8_t* data;
            VK_CHECK(vkMapMemory(device.vk_device, stagingBuffer.vk_deviceMemory, 0, stagingBuffer.vk_memoryRequirements.size, 0, (void**)&data));
            memcpy(data, ktx_texture->pData, ktx_texture->dataSize);
            vkUnmapMemory(device.vk_device, stagingBuffer.vk_deviceMemory);

            // Create Image to be Copied To 

            const VkImageCreateInfo imageCreateInfo = {
                .sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
                .imageType = VK_IMAGE_TYPE_2D,
                .format = format,
                .extent = {
                    .width = ktx_texture->baseWidth,
                    .height = ktx_texture->baseHeight,
                    .depth = 1 },
                .mipLevels = 1,
                .arrayLayers = 1,
                .samples = VK_SAMPLE_COUNT_1_BIT,
                .tiling = VK_IMAGE_TILING_OPTIMAL,
                .usage = VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
                .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
                .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED
            };

            device.createImage(imageCreateInfo, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, image);

            // Setup Copy 

            VkCommandPool commandPool;
            VkCommandBuffer commandBuffer;

            const VkCommandPoolCreateInfo commandPoolCreateInfo = {
                .sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
                .flags = VK_COMMAND_POOL_CREATE_TRANSIENT_BIT,
                .queueFamilyIndex = device.getGraphicsQueueFamilyIndex()
            };

            VK_CHECK(vkCreateCommandPool(device.vk_device, &commandPoolCreateInfo, nullptr, &commandPool));

            const VkCommandBufferAllocateInfo commandBufferAllocateInfo {
                .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
                .commandPool = commandPool,
                .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
                .commandBufferCount = 1
            };

            VK_CHECK(vkAllocateCommandBuffers(device.vk_device, &commandBufferAllocateInfo, &commandBuffer));

            const VkCommandBufferBeginInfo commandBufferBeginInfo {
                .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
                .flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
            };

            const VkBufferImageCopy bufferImageCopy {
                .bufferOffset = 4 * (512 * 256 + 256),
                .bufferRowLength = 512,
                .bufferImageHeight = 512,
                .imageSubresource = {
                    .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
                    .mipLevel = 0,
                    .baseArrayLayer = 0,
                    .layerCount = 1 },
                .imageOffset = { 256, 0, 0 },
                // .imageExtent = { ktx_texture->baseWidth, ktx_texture->baseHeight, 1 }
                .imageExtent = { 256, 256, 1 }
            };
            std::cout << "width: " << ktx_texture->baseWidth << " height: " << ktx_texture->baseHeight << '\n';

            const VkImageSubresourceRange subresourceRange {
                .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
                .baseMipLevel = 0,
                .levelCount = 1,
                .baseArrayLayer = 0,
                .layerCount = 1
            };

            const VkImageMemoryBarrier preCopyImageMemoryBarrier {
                .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
                .srcAccessMask = VK_ACCESS_HOST_WRITE_BIT,
                .dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT,
                .oldLayout = VK_IMAGE_LAYOUT_UNDEFINED,
                .newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
                .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
                .image = image.vk_image,
                .subresourceRange = subresourceRange
            };

            const VkImageMemoryBarrier postCopyImageMemoryBarrier {
                .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
                .srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT,
                .dstAccessMask = VK_ACCESS_SHADER_READ_BIT,
                .oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                .newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
                .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
                .image = image.vk_image,
                .subresourceRange = subresourceRange
            };

            // Record Copy 

            vkBeginCommandBuffer(commandBuffer, &commandBufferBeginInfo);

                vkCmdPipelineBarrier(
                    commandBuffer,
                    VK_PIPELINE_STAGE_HOST_BIT,
                    VK_PIPELINE_STAGE_TRANSFER_BIT,
                    0,
                    0, nullptr,
                    0, nullptr,
                    1, &preCopyImageMemoryBarrier);

                vkCmdCopyBufferToImage(
                    commandBuffer, 
                    stagingBuffer.vk_buffer, 
                    image.vk_image, 
                    VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 
                    1, &bufferImageCopy);

                vkCmdPipelineBarrier(
                    commandBuffer,
                    VK_PIPELINE_STAGE_TRANSFER_BIT,
                    VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
                    0,
                    0, nullptr,
                    0, nullptr,
                    1, &postCopyImageMemoryBarrier);

            vkEndCommandBuffer(commandBuffer);

            image.vk_imageLayout = postCopyImageMemoryBarrier.newLayout;

            // Submit Copy

            VkFence fence;

            const VkFenceCreateInfo fenceCreateInfo {
                .sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
            };

            VK_CHECK(vkCreateFence(device.vk_device, &fenceCreateInfo, nullptr, &fence));

            const VkSubmitInfo submitInfo {
                .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
                .waitSemaphoreCount = 0,
                .pWaitSemaphores = nullptr,
                .pWaitDstStageMask = nullptr,
                .commandBufferCount = 1,
                .pCommandBuffers = &commandBuffer,
                .signalSemaphoreCount = 0,
                .pSignalSemaphores = nullptr
            };

            VK_CHECK(vkQueueSubmit(device.getGraphicsQueue(), 1, &submitInfo, fence));
            VK_CHECK(vkWaitForFences(device.vk_device, 1, &fence, VK_TRUE, UINT64_MAX));

            // Cleanup

            vkDestroyFence(device.vk_device, fence, nullptr);
            vkFreeCommandBuffers(device.vk_device, commandPool, 1, &commandBuffer);
            vkDestroyCommandPool(device.vk_device, commandPool, nullptr);
            vkFreeMemory(device.vk_device, stagingBuffer.vk_deviceMemory, nullptr);
            vkDestroyBuffer(device.vk_device, stagingBuffer.vk_buffer, nullptr);
        }

        // Create Texture Image View, Sampler and Bind to Descriptor
        {
            const VkImageViewCreateInfo imageViewCreateInfo {
                .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
                .image = image.vk_image,
                .viewType = VK_IMAGE_VIEW_TYPE_2D,
                .format = image.vk_createInfo.format,
                .components = {
                    .r = VK_COMPONENT_SWIZZLE_IDENTITY,
                    .g = VK_COMPONENT_SWIZZLE_IDENTITY,
                    .b = VK_COMPONENT_SWIZZLE_IDENTITY,
                    .a = VK_COMPONENT_SWIZZLE_IDENTITY },
                .subresourceRange = {
                    .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
                    .baseMipLevel = 0,
                    .levelCount = 1,
                    .baseArrayLayer = 0,
                    .layerCount = 1 }
            };

            VK_CHECK(vkCreateImageView(device.vk_device, &imageViewCreateInfo, nullptr, &imageView));

            const VkSamplerCreateInfo samplerCreateInfo {
                .sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO,
                .magFilter = VK_FILTER_LINEAR,
                .minFilter = VK_FILTER_LINEAR,
                .mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR,
                .addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER,
                .addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER,
                .addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER,
                .mipLodBias = 0.0f,
                .anisotropyEnable = false,
                .maxAnisotropy = 0.0f,
                .compareEnable = VK_FALSE,
                .compareOp = VK_COMPARE_OP_ALWAYS,
                .minLod = 0.0f,
                .maxLod = 0.0f,
                .borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK,
                .unnormalizedCoordinates = VK_FALSE,
            };

            VK_CHECK(vkCreateSampler(device.vk_device, &samplerCreateInfo, nullptr, &sampler));

            const VkDescriptorImageInfo descriptorImageInfo {
                .sampler = sampler,
                .imageView = imageView,
                .imageLayout = image.vk_imageLayout,
            };

            VkWriteDescriptorSet writeDescriptorSet {
                .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
                .pNext = nullptr,
                .dstSet = descriptorManager.vk_descriptorSet,
                .dstBinding = 0,
                .dstArrayElement = 0,
                .descriptorCount = 1,
                .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                .pImageInfo = &descriptorImageInfo,
                .pBufferInfo = nullptr,
                .pTexelBufferView = nullptr,
            };

            vkUpdateDescriptorSets(device.vk_device, 1, &writeDescriptorSet, 0, nullptr);
        }

        // Create buffer for vertex data
        {
            // Put Texture Data in Staging Buffer

            vkmBuffer stagingBuffer;
            const VkDeviceSize bufferSize = static_cast<VkDeviceSize>(vertexData.size() * sizeof(VertexData));

            const VkBufferCreateInfo bufferCreateInfo = {
                .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
                .size  = bufferSize,
                .usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
                .queueFamilyIndexCount = 1,
                .pQueueFamilyIndices = &physicalDevice.graphicsQueueFamilyIndex
            };

            device.createBuffer(bufferCreateInfo, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, stagingBuffer);

            uint8_t* data;
            VK_CHECK(vkMapMemory(device.vk_device, stagingBuffer.vk_deviceMemory, 0, stagingBuffer.vk_memoryRequirements.size, 0, (void**)&data));
            memcpy(data, vertexData.data(), bufferCreateInfo.size);
            vkUnmapMemory(device.vk_device, stagingBuffer.vk_deviceMemory);

            // Create Image to be Copied To 

            const VkBufferCreateInfo deviceLocalBufferCreateInfo {
                .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
                .size  = bufferSize,
                .usage = VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
                .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
                .queueFamilyIndexCount = 1,
                .pQueueFamilyIndices = &physicalDevice.graphicsQueueFamilyIndex
            };

            device.createBuffer(deviceLocalBufferCreateInfo, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, vertexBuffer);

            // Setup Copy 

            VkCommandPool commandPool;
            VkCommandBuffer commandBuffer;

            const VkCommandPoolCreateInfo commandPoolCreateInfo = {
                .sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
                .flags = VK_COMMAND_POOL_CREATE_TRANSIENT_BIT,
                .queueFamilyIndex = device.getGraphicsQueueFamilyIndex()
            };

            VK_CHECK(vkCreateCommandPool(device.vk_device, &commandPoolCreateInfo, nullptr, &commandPool));

            const VkCommandBufferAllocateInfo commandBufferAllocateInfo {
                .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
                .commandPool = commandPool,
                .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
                .commandBufferCount = 1
            };

            VK_CHECK(vkAllocateCommandBuffers(device.vk_device, &commandBufferAllocateInfo, &commandBuffer));

            const VkCommandBufferBeginInfo commandBufferBeginInfo {
                .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
                .flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
            };

            const VkBufferCopy bufferCopy {
                .srcOffset = 0,
                .dstOffset = 0,
                .size = bufferSize,
            };

            const VkBufferMemoryBarrier preBufferMemoryBarrier {
                .sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER,
                .srcAccessMask = VK_ACCESS_HOST_WRITE_BIT,
                .dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT,
                .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
                .dstQueueFamilyIndex= VK_QUEUE_FAMILY_IGNORED,
                .buffer = vertexBuffer.vk_buffer,
                .offset = 0,
                .size = bufferSize
            };

            const VkBufferMemoryBarrier postBufferMemoryBarrier {
                .sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER,
                .srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT,
                .dstAccessMask = VK_ACCESS_SHADER_READ_BIT,
                .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
                .dstQueueFamilyIndex= VK_QUEUE_FAMILY_IGNORED,
                .buffer = vertexBuffer.vk_buffer,
                .offset = 0,
                .size = bufferSize
            };

            // Record Copy 

            vkBeginCommandBuffer(commandBuffer, &commandBufferBeginInfo);

                vkCmdPipelineBarrier(
                    commandBuffer,
                    VK_PIPELINE_STAGE_HOST_BIT,
                    VK_PIPELINE_STAGE_TRANSFER_BIT,
                    0,
                    0, nullptr,
                    1, &preBufferMemoryBarrier,
                    0, nullptr);

                vkCmdCopyBuffer(
                    commandBuffer,
                    stagingBuffer.vk_buffer,
                    vertexBuffer.vk_buffer,
                    1, &bufferCopy);

                vkCmdPipelineBarrier(
                    commandBuffer,
                    VK_PIPELINE_STAGE_TRANSFER_BIT,
                    VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
                    0,
                    0, nullptr,
                    1, &postBufferMemoryBarrier,
                    0, nullptr);

            vkEndCommandBuffer(commandBuffer);

            // Submit Copy

            VkFence fence;

            const VkFenceCreateInfo fenceCreateInfo {
                .sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
            };

            VK_CHECK(vkCreateFence(device.vk_device, &fenceCreateInfo, nullptr, &fence));

            const VkSubmitInfo submitInfo {
                .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
                .waitSemaphoreCount = 0,
                .pWaitSemaphores = nullptr,
                .pWaitDstStageMask = nullptr,
                .commandBufferCount = 1,
                .pCommandBuffers = &commandBuffer,
                .signalSemaphoreCount = 0,
                .pSignalSemaphores = nullptr
            };

            VK_CHECK(vkQueueSubmit(device.getGraphicsQueue(), 1, &submitInfo, fence));
            VK_CHECK(vkWaitForFences(device.vk_device, 1, &fence, VK_TRUE, UINT64_MAX));

            // Cleanup

            vkDestroyFence(device.vk_device, fence, nullptr);
            vkFreeCommandBuffers(device.vk_device, commandPool, 1, &commandBuffer);
            vkDestroyCommandPool(device.vk_device, commandPool, nullptr);
            vkFreeMemory(device.vk_device, stagingBuffer.vk_deviceMemory, nullptr);
            vkDestroyBuffer(device.vk_device, stagingBuffer.vk_buffer, nullptr);
        }
    }


    void destroyScene()
    {
        vkDestroySampler(device.vk_device, sampler, nullptr);
        vkDestroyImageView(device.vk_device, imageView, nullptr);
    }
};


int main()
{
    glfwInit();
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
    GLFWwindow* glfwWindow = glfwCreateWindow(WIN_WIDTH, WIN_HEIGHT, "Compute Test", nullptr, nullptr);

    //*** Instance 
    vkmInstance instance { glfwWindow };

    //*** Physical Device
    VkPhysicalDevice physicalDeviceHandle = VK_NULL_HANDLE;

    {
        uint32_t numPhysicalDevices = 0;
        vkEnumeratePhysicalDevices(instance.vk_instance, &numPhysicalDevices, nullptr);
        VkPhysicalDevice* physicalDevices = new VkPhysicalDevice[numPhysicalDevices];
        vkEnumeratePhysicalDevices(instance.vk_instance, &numPhysicalDevices, physicalDevices);

        physicalDeviceHandle = physicalDevices[1];
        delete [] physicalDevices;
    }

    vkmPhysicalDevice physicalDevice { physicalDeviceHandle, instance.getSurface() };
    vkmDevice device { physicalDevice };
    physicalDevice.show_vkPhysicalDeviceMemoryProperties();

    App app { physicalDevice, device, glfwWindow };

    glfwDestroyWindow(glfwWindow);
    glfwTerminate();

    return 0;
};