#ifndef VKM_DEVICE_HPP
#define VKM_DEVICE_HPP

#include <vulkan/vulkan.h>
#include <assert.h>

#include "vkmPhysicalDevice.hpp"


struct vkmBuffer
{
    VkBuffer vk_buffer;
    VkDeviceMemory vk_deviceMemory;
    VkMemoryRequirements vk_memoryRequirements;
};

struct vkmImage
{
    VkImage vk_image;
    VkDeviceMemory vk_deviceMemory;
    VkMemoryRequirements vk_memoryRequirements;

    VkImageCreateInfo vk_createInfo;
    VkImageLayout vk_imageLayout;
};


class vkmDevice
{
private:
    const vkmPhysicalDevice& physicalDevice;
    VkQueue graphicsQueue;

public:
    VkDevice vk_device;

    vkmDevice(const vkmPhysicalDevice& physDev) : physicalDevice(physDev) 
    {
        const char* deviceExtensions[1] = { VK_KHR_SWAPCHAIN_EXTENSION_NAME };
        const float queuePriority = 1.0f;

        const VkDeviceQueueCreateInfo queue = {
            .sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
            .queueFamilyIndex = physicalDevice.graphicsQueueFamilyIndex,
            .queueCount = 1,
            .pQueuePriorities = &queuePriority,
        };

        const VkDeviceCreateInfo deviceCreateInfo = {
                .sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
                .pNext = nullptr,
                .flags = 0x0,
                .queueCreateInfoCount = 1,
                .pQueueCreateInfos = &queue,
                .enabledLayerCount = 0,
                .ppEnabledLayerNames = nullptr,
                .enabledExtensionCount = 1,
                .ppEnabledExtensionNames = deviceExtensions,
                .pEnabledFeatures = nullptr,
        };

        VK_CHECK(vkCreateDevice(physicalDevice.vk_physicalDevice, &deviceCreateInfo, nullptr, &vk_device));

       vkGetDeviceQueue(vk_device, physicalDevice.graphicsQueueFamilyIndex, 0, &graphicsQueue);
    }

    ~vkmDevice()
    {
        vkDestroyDevice(vk_device, nullptr);
    }


    VkShaderModule createShaderModule(const char* filename)
    {
        FILE* f = fopen(filename, "r");
        if (f == NULL)
        {
            printf("Failed to open file %s!\n", filename);
            exit(EXIT_FAILURE);
        }

        fseek(f, 0, SEEK_END);
        const size_t nbytes_file_size = (size_t)ftell(f);
        rewind(f);

        uint32_t* buffer = (uint32_t*)malloc(nbytes_file_size);
        fread(buffer, nbytes_file_size, 1, f);
        fclose(f);

        VkShaderModuleCreateInfo ci_shader_module;
        ci_shader_module.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
        ci_shader_module.pNext = NULL;
        ci_shader_module.flags = 0x0;
        ci_shader_module.codeSize = nbytes_file_size;
        ci_shader_module.pCode = buffer;

        VkShaderModule vk_shader_module;
        VkResult result = vkCreateShaderModule(vk_device, &ci_shader_module, NULL, &vk_shader_module);
        if (result != VK_SUCCESS)
        {
            printf("Failed to create shader module for %s!\n", filename);
            exit(EXIT_FAILURE);
        }

        free(buffer);

        return vk_shader_module;
    }

    void createBuffer(const VkBufferCreateInfo& bufferCreateInfo, const VkMemoryPropertyFlags memoryPropertyFlags, vkmBuffer& buffer) const
    {
        VK_CHECK(vkCreateBuffer(vk_device, &bufferCreateInfo, nullptr, &buffer.vk_buffer));
        vkGetBufferMemoryRequirements(vk_device, buffer.vk_buffer, &buffer.vk_memoryRequirements);

        const VkMemoryAllocateInfo allocateInfo = { 
            .sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
            .allocationSize = buffer.vk_memoryRequirements.size,
            .memoryTypeIndex = physicalDevice.getHeapIndex(buffer.vk_memoryRequirements.memoryTypeBits, memoryPropertyFlags)
        };


        VK_CHECK(vkAllocateMemory(vk_device, &allocateInfo, nullptr, &buffer.vk_deviceMemory));
        VK_CHECK(vkBindBufferMemory(vk_device, buffer.vk_buffer, buffer.vk_deviceMemory, 0));
    }

    void createImage(const VkImageCreateInfo& imageCreateInfo, const VkMemoryPropertyFlags memoryPropertyFlags, vkmImage& image) const
    {
        VK_CHECK(vkCreateImage(vk_device, &imageCreateInfo, nullptr, &image.vk_image));
        vkGetImageMemoryRequirements(vk_device, image.vk_image, &image.vk_memoryRequirements);

        const VkMemoryAllocateInfo allocateInfo = { 
            .sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
            .allocationSize = image.vk_memoryRequirements.size,
            .memoryTypeIndex = physicalDevice.getHeapIndex(image.vk_memoryRequirements.memoryTypeBits, memoryPropertyFlags)
        };

        VK_CHECK(vkAllocateMemory(vk_device, &allocateInfo, nullptr, &image.vk_deviceMemory));
        VK_CHECK(vkBindImageMemory(vk_device, image.vk_image, image.vk_deviceMemory, 0));

        image.vk_createInfo = imageCreateInfo;
    }

    inline uint32_t getGraphicsQueueFamilyIndex() const { return physicalDevice.graphicsQueueFamilyIndex; }
    inline VkSurfaceKHR getSurface() const { return physicalDevice.vk_surfaceKHR; }
    inline VkQueue getGraphicsQueue() const { return graphicsQueue; }
};

#endif // VKM_DEVICE_HPP