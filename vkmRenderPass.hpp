#ifndef VKM_RENDERPASS_HPP
#define VKM_RENDERPASS_HPP

#define VK_CHECK( result ) do { \
   assert(result == VK_SUCCESS); \
} while ( 0 ) \

#include <vulkan/vulkan.h>

#include "vkmDevice.hpp"
#include "vkmSwapchain.hpp"

class vkmRenderPass
{
private:
    vkmDevice& device;
public:
    VkRenderPass vk_renderPass;

    vkmRenderPass(vkmDevice& _device)
    : device(_device)
    {}

    ~vkmRenderPass()
    {
        vkDestroyRenderPass(device.vk_device, vk_renderPass, nullptr);
    }

    void init(const vkmSwapchain& swapchain)
    {
        const VkAttachmentDescription attachments[1] {
            {   // Color
                .format = swapchain.getImageFormat(),
                .samples = VK_SAMPLE_COUNT_1_BIT,
                .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
                .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
                .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
                .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
                .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
                .finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
            }
        };

        const VkAttachmentReference color_reference {
            .attachment = 0,
            .layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
        };

        const VkSubpassDescription subpass {
            .pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS,
            .inputAttachmentCount = 0,
            .pInputAttachments = nullptr,
            .colorAttachmentCount = 1,
            .pColorAttachments = &color_reference,
            .pResolveAttachments = nullptr,
            .pDepthStencilAttachment = nullptr, 
            .preserveAttachmentCount = 0,
            .pPreserveAttachments = nullptr
        };

    	const VkSubpassDependency dependencies[2] {
            {
            	// First dependency at the start of the renderpass
            	// Does the transition from final to initial layout
            	.srcSubpass = VK_SUBPASS_EXTERNAL,                             // Producer of the dependency
            	.dstSubpass = 0,                                               // Consumer is our single subpass that will wait for the execution dependency
            	.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, // Match our pWaitDstStageMask when we vkQueueSubmit
            	.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, // is a loadOp stage for color attachments
            	.srcAccessMask = 0,                                            // semaphore wait already does memory dependency for us
            	.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,         // is a loadOp CLEAR access mask for color attachments
            	.dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT
            },
            {
            	// Second dependency at the end the renderpass
            	// Does the transition from the initial to the final layout
            	// Technically this is the same as the implicit subpass dependency, but we are gonna state it explicitly here
            	.srcSubpass = 0,                                               // Producer of the dependency is our single subpass
            	.dstSubpass = VK_SUBPASS_EXTERNAL,                             // Consumer are all commands outside of the renderpass
            	.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, // is a storeOp stage for color attachments
            	.dstStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT,          // Do not block any subsequent work
            	.srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,         // is a storeOp `STORE` access mask for color attachments
            	.dstAccessMask = 0,
            	.dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT
            }
        };
        
        const VkRenderPassCreateInfo renderPassCreateInfo {
            .sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
            .pNext = nullptr,
            .flags = 0x0,
            .attachmentCount = 1,
            .pAttachments = attachments,
            .subpassCount = 1,
            .pSubpasses = &subpass,
            .dependencyCount = 2,
            .pDependencies = dependencies
        };

        VK_CHECK(vkCreateRenderPass(device.vk_device, &renderPassCreateInfo, nullptr, &vk_renderPass));
    }
}; 

#endif // VKM_RENDERPASS_HPP