#ifndef VKM_INSTANCE_HPP
#define VKM_INSTANCE_HPP

#define VK_CHECK( result ) do { \
   assert(result == VK_SUCCESS); \
} while ( 0 ) \

#include <vulkan/vulkan.h>
#include <GLFW/glfw3.h>
#include <assert.h>

class vkmInstance
{
private:
    VkSurfaceKHR vk_surfaceKHR;
public:
    VkInstance vk_instance;

    vkmInstance(GLFWwindow* glfwWindow)
    {
        uint32_t numInstanceExtensions = 0;
        const char** instanceExtensions = glfwGetRequiredInstanceExtensions(&numInstanceExtensions);

        const uint32_t numInstanceLayers = 1;
        const char* instanceLayers[1] = { "VK_LAYER_KHRONOS_validation" };

        const VkApplicationInfo appInfo {
            .sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
            .pApplicationName = "Compute App",
            .applicationVersion = VK_MAKE_VERSION(1, 0, 0),
            .pEngineName = "Julia",
            .engineVersion = VK_MAKE_VERSION(1, 0, 0),
            .apiVersion = VK_API_VERSION_1_2,
        };

        const VkInstanceCreateInfo instanceCreateInfo {
            .sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
            .pApplicationInfo = &appInfo,
            .enabledLayerCount = numInstanceLayers,
            .ppEnabledLayerNames = instanceLayers,
            .enabledExtensionCount = numInstanceExtensions,
            .ppEnabledExtensionNames = instanceExtensions,
        };

        VK_CHECK(vkCreateInstance(&instanceCreateInfo, nullptr, &vk_instance));
        VK_CHECK(glfwCreateWindowSurface(vk_instance, glfwWindow, nullptr, &vk_surfaceKHR));
    }

    ~vkmInstance()
    {
        vkDestroySurfaceKHR(vk_instance, vk_surfaceKHR, nullptr);
        vkDestroyInstance(vk_instance, nullptr);
    }

    const VkSurfaceKHR& getSurface() { return vk_surfaceKHR; }
};


#endif // VKM_INSTANCE_HPP